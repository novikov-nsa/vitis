/* This file is part of Vitis.
 *
 * Copyright (C) 2018-2019 Eugene 'Vindex' Stulin
 *
 * Vitis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module module_vitis_base;
public alias moduleVitisBase = module_vitis_base;

public import amalthea.all, amalthea.phobos;
public import module_conf, module_output;

/*******************************************************************************
 * Valid commands for vitis.
 */
enum CommandEnum {
    e_show    = "show",
    e_open    = "open",
    e_run     = "run",
    e_create  = "create",
    e_assign  = "assign",
    e_delete  = "delete",
    e_service = "service",
    e_copy    = "copy",
    e_help    = "help",
};
alias CommandEnum CE;


/*******************************************************************************
 * Share directory
 */
immutable defaultShareDir = "/usr/share/help/";
static string shareDir = defaultShareDir;

string getShareDir() {
    return shareDir;
}
void setShareDir(string dirPath) {
    shareDir = dirPath;
}


/*******************************************************************************
 * The function returns path to main Vitis directory.
 */
string getVitisPath() {
    string vitisPath = VitisConf.getPath() ~ "/";
    if (vitisPath.exists && vitisPath.isFile) {
        auto msg = "(Error)"._s ~ " " ~ "file 'Vitis' blocks work."._s;
        throw new FileException(msg);
    } else if (!vitisPath.exists) {
        mkdirRecurse(vitisPath);
    }
    return vitisPath;
}


/*******************************************************************************
 * The function returns path to directory with desktop-files
 * (for Internet links) from Vitis.
 */
string getLinkEntriesPath() {
    auto vitisDesktopFilesPath = getVitisPath() ~ "__vitis/link_entries/";
    if (!exists(vitisDesktopFilesPath)) {
        mkdirRecurse(vitisDesktopFilesPath);
    }
    return vitisDesktopFilesPath;
}
string getHTMLLinkEntriesPath() {
    string path = getLinkEntriesPath ~ "html/";
    if (!exists(path)) {
        mkdirRecurse(path);
    }
    return path;
}
string getNonHTMLLinkEntriesPath() {
    string path = getLinkEntriesPath ~ "non-html/";
    if (!exists(path)) {
        mkdirRecurse(path);
    }
    return path;
}


string getFragPointersPath() {
    auto vitisFragPointersPath = getVitisPath() ~ "__vitis/fragpointers/";
    if (!exists(vitisFragPointersPath)) {
        mkdirRecurse(vitisFragPointersPath);
    }
    return vitisFragPointersPath;
}


string getAutoCategoriesPath() {
    return getVitisPath() ~ "__auto/";
}

string networkBookmarksCategory = "NetworkBookmarks";

string getRootNetworkBookmarksCategoryPath() {
    string path = getAutoCategoriesPath() ~ "N/";
    if (!exists(path)) {
        mkdirRecurse(path);
    }
    return path;
}
string getNetworkBookmarksCategoryPath() {
    string path = getRootNetworkBookmarksCategoryPath()
                ~ networkBookmarksCategory ~ "/";
    if (!exists(path)) {
        mkdirRecurse(path);
    }
    return path;
}

string fragPointersCategory() {
    return "Fragments"/*._s*/;
}

string getRootFragPointersCategoryPath() {
    string path = getAutoCategoriesPath() ~ "FP/";
    if (!exists(path)) {
        mkdirRecurse(path);
    }
    return path;
}
string getFragPointersCategoryPath() {
    string path = getRootFragPointersCategoryPath ~ fragPointersCategory ~ "/";
    if (!exists(path)) {
        mkdirRecurse(path);
    }
    return path;
}

string getRootFormatCategoryPath() {
    string path = getAutoCategoriesPath() ~ "F/";
    if (!exists(path)) {
        mkdirRecurse(path);
    }
    return path;
}
string formatCategory() { return "Format"/*._s*/; }
string getFormatCategoryPath() {
    string path = getRootFormatCategoryPath() ~ formatCategory ~ "/";
    if (!exists(path)) {
        mkdirRecurse(path);
    }
    return path;
}

string getRootTypeCategoryPath() {
    string path = getAutoCategoriesPath() ~ "T/";
    if (!exists(path)) {
        mkdirRecurse(path);
    }
    return path;
}
string typeCategory() { return "Type"/*._s*/; }
string getTypeCategoryPath() {
    string path = getRootTypeCategoryPath() ~ typeCategory ~ "/";
    if (!exists(path)) {
        mkdirRecurse(path);
    }
    return path;
}

string getRootExtensionCategoryPath() {
    string path = getAutoCategoriesPath() ~ "E/";
    if (!exists(path)) {
        mkdirRecurse(path);
    }
    return path;
}

string extensionCategory() {
    return "Extension"/*._s*/;
}

string getExtensionCategoryPath() {
    string path = getRootExtensionCategoryPath() ~ extensionCategory ~ "/";
    if (!exists(path)) {
        mkdirRecurse(path);
    }
    return path;
}

string[] getUsedAutoCategoriesPaths() {
    string[] modes = VitisConf.getAutocategorization().split(";").array;
    string[] paths;
    foreach(mode; modes) {
        if (mode == "none") break;
        if (mode.empty) continue;
        if (mode == "format") {
            paths ~= getFormatCategoryPath();
            paths ~= getTypeCategoryPath();
        } else if (mode == "extension") {
            paths ~= getExtensionCategoryPath();
        }
    }
    return paths;
}


bool isAutocategoryName(string name) {
    string topCategory = name.split('/')[0];
    if ("" == getPathIfAutocategory(topCategory)) {
        return false;
    }
    return true;
}


/*******************************************************************************
 * The function returns full path to directory of the automatic category.
 */
string getPathIfAutocategory(string category) {
    category = category.stripRight('/');
    string lowerCategory = category.toLower;

    if (VitisConf.getCaseSensitivity == "yes") {
        if (category == networkBookmarksCategory) {
            return getNetworkBookmarksCategoryPath;
        }
        if (category == fragPointersCategory) {
            return getFragPointersCategoryPath;
        }
    } else {
        if (lowerCategory == networkBookmarksCategory.toLower) {
            return getNetworkBookmarksCategoryPath;
        }
        if (lowerCategory == fragPointersCategory.toLower) {
            return getFragPointersCategoryPath;
        }
        {
            auto tempLang = amalthea.langlocal.getCurrentLanguage();
            scope(exit) amalthea.langlocal.chooseLanguage(tempLang);
            string[] languages = ["en_US", "ru_RU", "eo"];
            foreach(lang; languages) {
                amalthea.langlocal.chooseLanguage(lang);
                if (lowerCategory == formatCategory.toLower) {
                    return getFormatCategoryPath();
                }
                if (lowerCategory == typeCategory.toLower) {
                    return getTypeCategoryPath();
                }
                if (lowerCategory == extensionCategory.toLower) {
                    return getExtensionCategoryPath();
                }
            }
        }
    }

    string tryToFind(string autoRootDir) {
        string estimatedPath = autoRootDir ~ category;
        if (VitisConf.getCaseSensitivity == "yes") {
            if (estimatedPath.exists) {
                return estimatedPath;
            }
        } else {
            string[] dirsInAutoDir = amalthea.fs.getDirListRecurse(autoRootDir);
            foreach(p; dirsInAutoDir) {
                if (estimatedPath.toLower == p.toLower) {
                    return p;
                }
            }
        }
        return "";
    }

    string[] autoCatMode = VitisConf.getAC().split(';').array;
    string path;
    string autoDir;
    if (canFind(autoCatMode, "format")) {
        //autocategory by format
        autoDir = getRootFormatCategoryPath();
        path = tryToFind(autoDir);
        if (path != "") return path;
        //autocategory by type
        autoDir = getRootTypeCategoryPath();
        path = tryToFind(autoDir);
        if (path != "") return path;
    } else if (canFind(autoCatMode, "extension")) {
        //autocategory by extension
        autoDir = getRootExtensionCategoryPath();
        path = tryToFind(autoDir);
        if (path != "") return path;
    }

    return "";
}


/*******************************************************************************
 * The function returns full path to directory of the category.
 */
string getCatDirPath(string category) {
    category = category.stripRight('/');
    string estimatedPath = getVitisPath ~ category ~ '/';
    if (exists(estimatedPath)) {
        return estimatedPath;
    }
    if (VitisConf.getCaseSensitivity == "yes") {
        string lowerCategory = category.toLower;
        string[] allPathsToCategories
            = amalthea.fs.getDirListRecurse(getVitisPath)
                .filter!(a => !a.startsWith(getVitisPath ~ "__"))
                .array;
        foreach(p; allPathsToCategories) {
            if (lowerCategory == baseName(p).toLower) {
                return p ~ '/';
            }
        }
    }
    return getPathIfAutocategory(category);
}
deprecated alias getCategoryPath = getCatDirPath;


/*******************************************************************************
 * The function checks whether the argument is subcategory (of other category)
 * with global scope.
 */
bool isGlobalSubCategory(string thing) {
    thing = std.algorithm.strip(thing, '/');
    if (!thing.canFind("/")) { //only: cat/subcat
        return false;
    }
    auto fullPath = getCatDirPath(thing);
    auto globPath = getCatDirPath(thing.baseName);
    if (fullPath.empty || globPath.empty) return false;
    if (globPath.isSymlink && globPath.readLink == fullPath) {
        return true;
    }
    return false;
}


string[] getTopLevelCategories(Flag!"fullPath" fullPath = Yes.fullPath) {
    auto categories = getTopLevelCategoriesAndAliases()
            .filter!(a => !a.isSymlink)
            .array;
    if (!fullPath) {
        size_t l = getVitisPath().length;
        foreach(ref cat; categories) {
            cat = cat[l .. $];
        }
    }
    return categories;
}

string[] getTopLevelCategoriesAndAliases(
    Flag!"fullPath" fullPath = Yes.fullPath
) {
    string[] usercategories = amalthea.fs.getDirList(getVitisPath)
        .filter!(a => !a.canFind("/__") && a.isDir)
        .array;
    if (!fullPath) {
        size_t l = getVitisPath().length;
        foreach(ref cat; usercategories) {
            cat = cat[l .. $];
        }
    }
    string[] autocategories = getTopLevelAutoCategories(fullPath);
    return usercategories ~ autocategories;
}

string[] getAllCategoriesAndAliases(Flag!"fullPath" fullPath = No.fullPath) {
    //exclude unnecessary catalogies for repeared files and service catalogies
    auto categories = amalthea.fs.getDirListRecurse(getVitisPath)
        .filter!(a => !a.baseName.startsWith("__"))
        .filter!(a => !a.directoryOfFilePath.startsWith("__"))
        .array;
    if (!fullPath) {
        size_t l = getVitisPath().length;
        foreach(ref cat; categories) {
            cat = cat[l .. $];
        }
    }
    return categories ~ getAllAutoCategories(fullPath);
}


string[] getAliases(Flag!"fullPath" fullPath = No.fullPath) {
    //exclude unnecessary catalogies for repeared files and service catalogies
    auto categories = amalthea.fs.getDirListRecurse(getVitisPath)
        .filter!(a => !a.baseName.startsWith("__"))
        .filter!(a => !a.directoryOfFilePath.startsWith("__"))
        .filter!(a => a.isSymlink)
        .array;
    if (!fullPath) {
        size_t l = getVitisPath().length;
        foreach(ref cat; categories) {
            cat = cat[l .. $];
        }
    }
    return categories;
}


string[] getTopLevelAutoCategories(Flag!"fullPath" fullPath = No.fullPath) {
    if (!getAutoCategoriesPath().exists) return null;
    return amalthea.fs.getDirList(getAutoCategoriesPath);
}

string[] getAllAutoCategories(Flag!"fullPath" fullPath = Yes.fullPath) {
    string[] autoCategories;
    void subfn(string autoDirPath) {
        if (!autoDirPath.exists) return;
        autoCategories ~= autoDirPath;
        string[] categories = amalthea.fs.getDirListRecurse(autoDirPath)
            .filter!(a => !a.baseName.startsWith("__"))
            .filter!(a => !a.directoryOfFilePath.startsWith("__"))
            .array;
        autoCategories ~= categories;
    }
    subfn(getFormatCategoryPath);
    subfn(getTypeCategoryPath);
    subfn(getExtensionCategoryPath);
    if (fullPath) {
        return autoCategories;
    }
    size_t l = getAutoCategoriesPath().length;
    foreach(ref cat; autoCategories) {
        cat = cat[l+2 .. $]; //remove parts of paths 'F/', 'E/', 'T/'
    }
    return autoCategories;
}


/*******************************************************************************
 * The function returns the real name of the category by its alias
 */
string getCategoryByAlias(string catAlias)
in {
    assert(getCatDirPath(catAlias).stripRight('/').isSymlink);
}
body {
    string aliasPath = getCatDirPath(catAlias).stripRight('/');
    string categoryPath = readLink(aliasPath);
    return categoryPath[getVitisPath.length .. $];
}


/*******************************************************************************
 * Returns opening application depending on the desktop environment
 */
deprecated string chooseOpenerByDefault() {
    string vitisDefaultTool = "vts-fs-open";
    if (amalthea.sys.getAppPath(vitisDefaultTool)) {
        return vitisDefaultTool;
    }
    return "";
}

bool stringIsUint(string value) {
    import std.uni : isNumber;
    foreach(c; value) if (!(isNumber(to!dchar(c)))) return false;
    return true;
}

/*******************************************************************************
 * Function returns chopped (rarefied) array 
 * by numbers like "1-4,6,8-11,9" or simple number
 */
string[] getRarefiedArrayByNumbers(string[] arr, string numbers) {
    import std.regex;
    string[] resultArray;
    resultArray.length = arr.length;
    if (stringIsUint(numbers)) {
        ssize_t n = to!ssize_t(numbers)-1;
        if (n >= 0 && n < resultArray.length) resultArray[n] = arr[n];
        return resultArray;
    }

    //for valid expression, for example: [1-5,7,12,14-19]
    auto r = ctRegex!(r"^\[[0-9]+(-[0-9]+)?(,+[0-9]+(-[0-9]+)?)*\]$");
    numbers = std.string.replace(numbers, ",", ",,");
    if (!numbers.startsWith("[") && !numbers.endsWith("]")) {
        numbers = "[" ~ numbers ~ "]"; //legacy
    }
    auto c = numbers.matchFirst(r);
    if (0 != c.length) {
        immutable
            strRegExpForRanges = `((?P<index1>[0-9]+)-(?P<index2>[0-9]+))`,
            strRegExpForSimpleNumber = `([,\[](?P<index>[0-9]+)[,\]])`;
        auto sharedRegExp
            = ctRegex!(strRegExpForRanges ~ "|" ~ strRegExpForSimpleNumber);

        foreach (el; matchAll(c[0], sharedRegExp)) {
            try {
                for (auto i = el["index1"].to!size_t-1;
                     i < el["index2"].to!size_t;
                     ++i) {
                    if (i >= 0 && i < resultArray.length)
                        resultArray[i] = arr[i];
                }
            }
            //if el["index1"] is invalid
            catch(std.conv.ConvException e) {
                auto index = el["index"].to!size_t-1;
                resultArray[index] = arr[index];
            }
        }
    } else {
        throw new ValidException("Incorrect use of option '-n'."._s);
    }
    return resultArray;
}


/*******************************************************************************
 * The function checks whether the word is an option (flag)
 */
bool isOption(string line) {
    return line.startsWith("-");
}


string[string] getLinksFromCategory(string category) {
    auto dir = getCatDirPath(category);
    return amalthea.fs.getSymlinksInfo(dir);
}


/*******************************************************************************
 * The function returns an associative array with full paths to links (as keys)
 * and file paths (as values) from the Vitis
 */
string[string] getAllLinks() {
    string[] allTopCategories = getTopLevelCategories();
    string[string] allLinks;
    foreach(category; allTopCategories) {
        auto temp = amalthea.fs.getSymlinksInfo(category); //recursively
        foreach(k, v; temp) allLinks[k] = v;
    }
    return allLinks;
}


deprecated string[] findFilesInVitis(string file) {
    string[] categories = getTopLevelCategories(No.fullPath);
    string[] filepaths;
    foreach(c; categories) {
        string vitisFile = c ~ "/" ~ file;
        writeln("vitisFile = ", vitisFile);
        try filepaths ~= findFileInVitis(vitisFile)[0];
        catch (Exception e) {writeln(e.msg);}
    }
    return filepaths;
}

auto findFileInVitis(string vitisFile,
                     size_t number = 0) {
    string finalFilepath;
    string finalLinkpath;
    string category = directoryOfFilePath(vitisFile).stripRight('/');
    enforce(!category.empty);
    string linkName = fileNameFromFilePath(vitisFile);
    
    string[string] files;
    files = getFileListByExpression([category], Yes.fullLinkPath);
    size_t counter;
    string filepath, linkpath;
    foreach(f, l; files) {
        if (l.baseName == linkName) {
            counter++;
            filepath = f;
            linkpath = l;
        }
    }
    if (0 == counter) {
        auto msg = category~"/"~linkName~": file in Vitis is not found."._s;
        throw new FileWarning(msg);
    } else if (1 == counter) {
        if (number != 0 && number != 1) {
            auto msg = "File with this number not found."._s;
            throw new FileWarning(msg);
        }
        finalFilepath = filepath;
        finalLinkpath = linkpath;
    } else {
        if (number == 0) {
            auto msg = (s_("Specify the number of the required file " ~
                        "using the '--number' flag."));
            throw new FileWarning(msg);
        }
        size_t[string] numbersOfRepeatedFiles =
            groupByFilePathWithNumbers(files);
        foreach(linkpath, n; numbersOfRepeatedFiles) {
            if (n == number && linkpath.baseName == linkName) {
                finalFilepath = linkpath.readLink;
                finalLinkpath = linkpath;
                break;
            }
        }
        if (finalFilepath.empty) {
            auto msg = "File with this number not found."._s;
            throw new FileWarning(msg);
        }
    }
    Tuple!(string, "filepath", string, "linkpath") result;
    result.filepath = finalFilepath;
    result.linkpath = finalLinkpath;
    return result;
}


/*******************************************************************************
 * The function returns link list (keys are linked files, values are links)
 */
string[string] getFileListByExpression(
                    string[] expression,
                    Flag!"fullLinkPath" fullLinkPath = No.fullLinkPath) {
    //remove unwanted slashes
    for (size_t i; i < expression.length; i++) {
        expression[i] = expression[i].strip('/');
    }
    import module_RPN;
    string[string] rawfiles = module_RPN.calculateExpression(expression);
    string[string] files;

    if (fullLinkPath) {
        foreach(fpath, lpath; rawfiles) {
            files[fpath] = lpath; //copying?
        }
    } else {
        foreach(fpath, lpath; rawfiles) {
            files[fpath] = baseName(lpath);
        }
    }
    return files;
}


size_t[string] groupByFilePathWithNumbers(string[string] filesWithLinks) {
    import amalthea.dataprocessing : orderMapByKeys;
    string[] files, links;
    size_t[] numbers;
    size_t[string] numbersOfRepeatedNames;
    orderMapByKeys(filesWithLinks, files, links);
    numbers.length = links.length;

    size_t[string] repeatedNamesCounter;
    foreach(i; 0 .. numbers.length) {
        auto lpath = links[i];
        repeatedNamesCounter[lpath.baseName]++;
    }
    foreach(name, n; repeatedNamesCounter) {
        size_t internalCounter;
        foreach(i, fpath; files) {
            auto lpath = links[i];
            if (lpath.baseName == name) {
                if (n == 1) {
                    numbersOfRepeatedNames[lpath] = 0;
                    break;
                } else {
                    internalCounter++;
                    numbersOfRepeatedNames[lpath] = internalCounter;
                }
            }
        }
    }
    return numbersOfRepeatedNames;
}


deprecated size_t getNumberOfRepeatedFile(string linkPath) {
    string[] pathParts = linkPath.split('/').array;
    if (pathParts[$-3] == "__repeated_names") {
        return pathParts[$-2].to!size_t;
    }
    return 1;
}


/*******************************************************************************
 * Decorative border for use in user interface
 */
string getBorder() {
    char[] arr;
    arr.length = 80;
    arr[] = '-';
    string border = to!string(arr) ~ "\n";
    return border;
}


/*******************************************************************************
 * Interactive element of UI: getting an answer (yes/no) to a question
 */
bool getAnswerToTheQuestion(string question) {
    "simple".tprint(question);
    string ans = readln;
    ans = std.string.strip(ans).toLower;
    if (ans.among("y"._s, "yes"._s, "y", "yes")) {
        return true;
    }
    return false;
}


/*******************************************************************************
 * The function is used in the beginning of unit test,
 * denotes the lower bound of the test.
 */
void utBegin(string place) {
    writeln("========================================");
    writeln(">: unittesting in ", place, "\n");
}


/*******************************************************************************
 * The function is used in the end of unit test,
 * denotes the upper bound of the test.
 */
void utEnd(string place) {
    writeln("\n>: end of unittesting in ", place);
    writeln("========================================\n");
}


/*******************************************************************************
 * The function checks condition and throws exception if the condition is false
 */
void checkCondition(alias pred)(bool thing,
                                string file = __FILE__,
                                size_t line = __LINE__)
  if (is(typeof(pred) == string))
{
    if (!thing) {
        //stderr.writeln(file, ": ", line);
        throw new ValidException(pred, file, line);
    }
}


/*******************************************************************************
 * The function deletes duplicate slashes in a string.
 */
void removeDuplicateSlashes(ref string s) {
    while(s.canFind("//")) {
        s = s.replace("//", "/");
    }
}
unittest {
    auto line = "/home/user//Documents///file.odt";
    line.removeDuplicateSlashes();
    assert(line == "/home/user/Documents/file.odt");
}


/*******************************************************************************
 * The function converts an expression of the form 'cat1/cat2'
 * to an array of the form ["cat1", "i:", "cat2"].
 */
deprecated string[] convertExpressionWithSlash(string expression) {
    removeDuplicateSlashes(expression);
    expression = expression.replace("/", "/i:/");
    string[] arr = expression.split("/");
    return arr;
}
unittest {
    auto query = "Music/Foreign/80s";
    auto alteredQuery = ["Music", "i:", "Foreign", "i:", "80s"];
    assert(convertExpressionWithSlash(query) == alteredQuery);
}


/// Template for exceptions
private mixin template RealizeException() {
    this(string msg, string file = __FILE__, size_t line = __LINE__) {
        super(msg, file, line);
    }
}
/// Exception related to validation.
class ValidException : Exception { mixin RealizeException; }
/// Exception for file system problems.
class FileWarning : Exception { mixin RealizeException; }
/// Exception for file space problems.
class FileSpaceException : Exception { mixin RealizeException; }
/// Exception for nonexistent programs.
class AppNotFound : Exception { mixin RealizeException; }

/*******************************************************************************
 * The function prints error message.
 */
void printErrorMessage(string msg) {
    "error".tprint("(Error)"._s);
    stderr.writeln(" ", msg);
}
deprecated alias exceptionHandler = printErrorMessage;
