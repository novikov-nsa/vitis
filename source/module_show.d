/* This file is part of Vitis.
 *
 * Copyright (C) 2018-2019 Eugene 'Vindex' Stulin
 *
 * Vitis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

module module_show;
public alias moduleShow = module_show;

import module_vitis_base,
       module_help,
       module_RPN;

struct MainOptions {
    string numbers;
    SortEnum sortEnum;
    bool reverse;
    bool mix;
    bool hidden;
}
MainOptions extractMainOptions(ref string[] args) {
    MainOptions mainOptions;
    mainOptions.numbers = args.extractOptionValue("-n", "--numbers");
    mainOptions.sortEnum = SortEnum.name;
    getopt(args, config.passThrough, "sort",    &mainOptions.sortEnum,
                                     "reverse", &mainOptions.reverse,
                                     "mix",     &mainOptions.mix,
                                     "hidden",  &mainOptions.hidden);
    return mainOptions;
}


struct ShowingOptions {
    bool categories;
    bool details;
    bool paths;
    bool ipaths;
    bool fragmentInfo;
    bool noColors;
    bool noNumbers;
    bool machineOutput;
}
ShowingOptions extractShowingOptions(ref string[] args) {
    ShowingOptions options;
    if (!stdout.isTTY) options.noColors = true;
    getopt(args, config.passThrough, "categories",    &options.categories,
                                     "details",       &options.details,
                                     "paths",         &options.paths,
                                     "ipaths",        &options.ipaths,
                                     "fragment-info", &options.fragmentInfo,
                                     "no-colors",     &options.noColors,
                                     "no-numbers",    &options.noNumbers,
                                     "m|machine",     &options.machineOutput);
    if (options.machineOutput) {
        options.noNumbers = true;
        options.noColors = true;
    }
    return options;
}


struct OpeningOptions {
    string app;
    bool savedPage;
}
OpeningOptions extractOpeningOptions(ref string[] args) {
    OpeningOptions options;
    getopt(args, config.passThrough, "app",        &options.app,
                                     "saved-page", &options.savedPage);
    return options;
}


struct RunningOptions {
    bool sudo;
    string user;
}
RunningOptions extractRunningOptions(ref string[] args) {
    RunningOptions options;
    getopt(args, config.passThrough, "sudo", &options.sudo,
                                     "user", &options.user);
    return options;
}

deprecated bool flagHidden;
deprecated string msgWrongUsing;


/*******************************************************************************
 * The function manages the commands "show", "open", "run"
 */
void mainFn(string[] args) {
    CE command = cast(CE)args[0];
    string err = s_("Wrong using of command '" ~ cast(string)command ~ "'.");
    checkCondition!err(args.length >= 2);

    if (args[1].among("--help", "-h")) {
        fnHelp(cast(string)command);
        return;
    }


    bool modeCategoriesList;
    getopt(args, config.passThrough, "all-categories",  &modeCategoriesList);
    if (command == CE.e_show && modeCategoriesList) {
        bool showAutoCategories;
        getopt(args, config.passThrough, "auto", &showAutoCategories);
        bool hidden;
        getopt(args, config.passThrough, "hidden", &hidden);
        checkCondition!err(args.length == 1);
        showAllCategories(hidden, showAutoCategories);
        return;
    }

    if (!args[1].isOption) {
        args = args[0] ~ ["-e"] ~ args[1 .. $];
    }

    bool showingMode, openingMode, runningMode;

    MainOptions mainOptions = extractMainOptions(args);
    ShowingOptions showingOptions;
    OpeningOptions openingOptions;
    RunningOptions runningOptions;
    if (command == CE.e_show) {
        showingOptions = extractShowingOptions(args);
        getopt(args, config.passThrough, "open", &openingMode,
                                       "run",  &runningMode);
        if (openingMode) openingOptions = extractOpeningOptions(args);
        if (runningMode) runningOptions = extractRunningOptions(args);
    } else if (command == CE.e_open) {
        openingOptions = extractOpeningOptions(args);
        getopt(args, config.passThrough, "show", &showingMode);
        if (showingMode) showingOptions = extractShowingOptions(args);
    } else if (command == CE.e_run) {
        runningOptions = extractRunningOptions(args);
        getopt(args, config.passThrough, "show", &showingMode);
        if (showingMode) showingOptions = extractShowingOptions(args);
    }
    
    string[] expression = args.extractOptionRange("-e");
    if (!expression.empty) {
        checkCondition!err(args.length == 1);
        if (command == CE.e_show) {
            showFilesByExpression(expression, mainOptions, showingOptions);
            if (openingMode) {
                openFilesByExpression(expression, mainOptions, openingOptions);
            } else if (runningMode) {
                runFilesByExpression(expression, mainOptions, runningOptions);
            }
        } else if (command == CE.e_open) {
            openFilesByExpression(expression, mainOptions, openingOptions);
            if (showingMode) {
                showFilesByExpression(expression, mainOptions, showingOptions);
            }
        } else if (command == CE.e_run) {
            runFilesByExpression(expression, mainOptions, runningOptions);
            if (showingMode) {
                showFilesByExpression(expression, mainOptions, showingOptions);
            }
        }
        return;
    }

    string vitisFile = args.extractOptionValue("-v");
    if (!vitisFile.empty) {
        checkCondition!err(!directoryOfFilePath(vitisFile).empty);
        size_t number;
        string strNumber = args.extractOptionValue("--number");
        number = strNumber.empty ? 0 : strNumber.to!size_t;
        checkCondition!err(args.length == 1);
        if (command == CE.e_show) {
            showVitisFile(vitisFile, mainOptions, showingOptions, number);
        } else if (command == CE.e_open) {
            openVitisFile(vitisFile, mainOptions, openingOptions, number);
        } else if (command == CE.e_run) {
            runVitisFile(vitisFile, mainOptions, runningOptions, number);
        }
        return;
    }
}


void showAllCategories(bool flagHidden, bool flagAutocategories) {
    string[] allRootAutoCatPaths = [
        //getAutoCategoriesPath(),
        getRootNetworkBookmarksCategoryPath(),
        getRootFragPointersCategoryPath(),
        getRootFormatCategoryPath(),
        getRootTypeCategoryPath(),
        getRootExtensionCategoryPath()
    ];

    string[] dirList;
    string[] catList;
    string vtspath = getVitisPath();
    auto tempDirList = amalthea.fs.getDirListRecurse(vtspath).array;
    tempDirList = std.algorithm.sort(tempDirList).array;
    foreach(dir; tempDirList) {
        if (dir.canFind("__repeated_names")) continue;
        string catName = dir[vtspath.length .. $];
        if (catName.startsWith("__")) {
            if (!flagAutocategories) continue;
            if (catName.startsWith("__auto")) {
                if (catName == "__auto") continue;
                foreach(p; allRootAutoCatPaths) {
                    if (p == dir) continue;
                    if (dir.startsWith(p)) {
                        catName = dir[p.length .. $];
                        catList ~= catName;
                        dirList ~= dir;
                    }
                }
                continue;
            }
        }
        catList ~= catName;
        dirList ~= dir;
    }
    foreach(i; 0 .. catList.length) {
        string dir = dirList[i];
        string category = catList[i];
        string subcategory = category.baseName;
        if (!flagHidden && subcategory.startsWith(".")) continue; 
        string outputLine = `"`~category~`"`;
        if (dir.isSymlink) {
            auto origPath = readLink(dir);
            auto l = getVitisPath().length;
            auto origCategory = origPath[l .. $].strip('/');
            outputLine ~= " -> " ~ `"`~origCategory~`"`;
            /*
            if (readLink(dir).baseName != category) {
                outputLine ~= " -> " ~ `"`~readLink(dir).baseName~`"`;
            }*/
        }
        "simple".tprintln(outputLine);
    }
}


void showFilesByExpression(string[] expression,
                           MainOptions mainOptions,
                           ShowingOptions opts) {
    string[string] files;
    files = getFileListByExpression(expression, Yes.fullLinkPath);
    showFilesByFileList(files, mainOptions, opts);
}


void showFilesByFileList(string[string] fileList,
                         MainOptions mainOptions,
                         ShowingOptions opts) {
    setColorStdout(!opts.noColors);
    setColorStderr(!opts.noColors);

    string[string] tempFiles = fileList.dup;
    string[string] files;
    foreach(f, l; tempFiles) {
        if (!mainOptions.hidden && l.baseName.startsWith(".")) continue;
        files[f] = l;
    }
    size_t[string] numbersOfRepeatedNames = groupByFilePathWithNumbers(files);
    string[] filepaths, linkpaths;
    sortBySortEnum(mainOptions.sortEnum, files, filepaths, linkpaths);

    string[] printableLinkPaths, printableFilePaths;
    ssize_t maxNumber;
    if (!mainOptions.numbers.empty) {
        printableLinkPaths = getRarefiedArrayByNumbers(linkpaths,
                                                       mainOptions.numbers);
        printableFilePaths = getRarefiedArrayByNumbers(filepaths,
                                                       mainOptions.numbers);
        foreach(i, l; printableLinkPaths) if ("" != l) maxNumber = i+1;
    } else {
        maxNumber = linkpaths.length;
        printableLinkPaths = linkpaths;
        printableFilePaths = filepaths;
    }

    for (ssize_t index; index < printableLinkPaths.length; index++) {
        ssize_t i = index;
        if (mainOptions.reverse) i = printableLinkPaths.length - 1 - index;
        string linkPath = printableLinkPaths[i];
        if (linkPath == "") continue;
        auto linkName = linkPath.baseName;
        string filePath = printableFilePaths[i];
        auto noNumbers = opts.noNumbers;
        auto machine = opts.machineOutput;

        auto numberOfRepeating = numbersOfRepeatedNames[linkPath];
        printLinkNameLine(linkName, i+1, maxNumber, noNumbers,
                          machine, numberOfRepeating);
        if (opts.paths)        printFilePath(filePath, machine);
        if (opts.ipaths)       printNetPath(filePath);
        if (opts.details)      printDetails(filePath, opts.noColors);
        if (opts.fragmentInfo) printFragmentInfo(filePath);
        if (opts.categories)   printCategories(filePath, opts.noColors);
    }
}


void openFilesByExpression(string[] expression,
                           MainOptions mainOptions,
                           OpeningOptions openingOptions) {
    string[string] files = getFileListByExpression(expression);
    openFilesByFileList(files, mainOptions, openingOptions);
}
void openFilesByFileList(string[string] fileList,
                         MainOptions mainOptions,
                         OpeningOptions openingOptions) {
    if (openingOptions.app.empty) {
        openingOptions.app = VitisConf.getOpener();
    }
    string[string] tempFiles = fileList.dup;
    string[string] files;
    foreach(f, l; tempFiles) {
        if (!mainOptions.hidden && l.startsWith(".")) continue;
        files[f] = l;
    }
    string[] filepaths, linkpaths;
    sortBySortEnum(mainOptions.sortEnum, files, filepaths, linkpaths);
    string[] resultFilePaths;
    if (!mainOptions.numbers.empty) {
        resultFilePaths = getRarefiedArrayByNumbers(filepaths,
                                                    mainOptions.numbers);
    } else {
        resultFilePaths = filepaths;
    }
    if (mainOptions.reverse) {
        std.algorithm.reverse(resultFilePaths);
    }
    if (mainOptions.mix) {
        import std.random : randomShuffle;
        randomShuffle(resultFilePaths);
    }

    string[] command = [openingOptions.app];
    foreach(f; resultFilePaths) {
        if ("" == f) continue;
        if (f.isApplication) {
            auto msg = f ~ s_(": this file is an application. "
                    ~ "Perhaps it is not intended to be opened.");
            "warning".tprintln(msg);
        }
        if (f.endsWith(".desktop")) {
            if (!openingOptions.savedPage) {
                f = getURLFromDesktopFile(f);
            } else {
                auto page = dirName(f) ~ f.baseName.stripExtension ~ ".html";
                if (!page.exists) {
                    auto msg = page ~ ": saved page is not found."._s;
                    "warning".tprintln(msg, " ", "Skipped."._s);
                    continue;
                }
                f = page;
            }
        }
        command ~= f;
    }
    //execute(command);
    string strCmd = command[0];
    foreach(el; command[1 .. $]) {
        el = el.replace(`"`, `\"`);
        strCmd ~= " " ~ `"`~el~`"`;
    }
    c_system(strCmd);
}


void runFilesByExpression(string[] expression,
                          MainOptions mainOptions,
                          RunningOptions runningOptions) {
    string[string] files = getFileListByExpression(expression);
    runFilesByFileList(files, mainOptions, runningOptions);
}
void runFilesByFileList(string[string] fileList,
                        MainOptions mainOptions,
                        RunningOptions runningOptions) {
    executeShell("xhost si:localuser:root"); //important if Wayland
    bool sudo = runningOptions.sudo;
    string user = runningOptions.user;

    string[] precmd;
    if (sudo && !user.empty) {
        precmd = ["sudo", "user="~user];
    } else if (sudo) {
        precmd = ["sudo"];
    } else if (!user.empty) {
        precmd = ["su", user, "-c"];
    }

    string[string] tempFiles = fileList.dup;
    string[string] files;
    foreach(f, l; tempFiles) {
        if (!mainOptions.hidden && l.startsWith(".")) continue;
        files[f] = l;
    }
    string[] filepaths, linkpaths;
    sortBySortEnum(mainOptions.sortEnum, files, filepaths, linkpaths);
    string[] resultFilePaths;
    if (!mainOptions.numbers.empty) {
        resultFilePaths = getRarefiedArrayByNumbers(filepaths,
                                                    mainOptions.numbers);
    } else {
        resultFilePaths = filepaths;
    }
    if (mainOptions.reverse) {
        std.algorithm.reverse(resultFilePaths);
    }

    string[] command = precmd;
    foreach(app; resultFilePaths) {
        if (app.empty) continue;
        command ~= app;
        execute(command);
    }
}


void showVitisFile(string vitisFile,
                   MainOptions mainOptions,
                   ShowingOptions showingOptions,
                   size_t number) {
    auto res = findFileInVitis(vitisFile, number);
    string[string] files = [res[0]:res[1]];
    showFilesByFileList(files, mainOptions, showingOptions);
}


void openVitisFile(string vitisFile,
                   MainOptions mainOptions,
                   OpeningOptions openingOptions,
                   size_t number) {
    auto res = findFileInVitis(vitisFile, number);
    string[string] files = [res[0]:res[1]];
    openFilesByFileList(files, mainOptions, openingOptions);
}


void runVitisFile(string vitisFile, 
                  MainOptions mainOptions,
                  RunningOptions runningOptions,
                  size_t number) {
    auto res = findFileInVitis(vitisFile, number);
    string[string] files = [res[0]:res[1]];
    runFilesByFileList(files, mainOptions, runningOptions);
}


private void printLinkNameLine(string linkName,
                               ssize_t currNumber,
                               ssize_t maxNumber,
                               bool noNumbers,
                               bool machineOutput,
                               size_t numberOfRepeating) {
    if (machineOutput) {
        noNumbers = true;
        linkName = `"`~linkName~`"`;
    }
    auto numberOfDigits = to!string(maxNumber).length;
    auto numberOfCurrentDigits = to!string(currNumber).length;
    if (!noNumbers) {
        auto numberOfSpaces = numberOfDigits - numberOfCurrentDigits;
        for (size_t j; j < numberOfSpaces; j++) write(" ");
        "simple".tprint("[", currNumber, "] ");
    }
    if (numberOfRepeating) {
        "simple".tprint(numberOfRepeating, "/");
    }
    "filename".tprintln(linkName);
}


private bool isApplication(string filepath) {
    string[] cmd = ["xdg-mime", "query", "filetype", filepath];
    auto res = execute(cmd);
    return res.output == "application/x-executable";
}


private void printFilePath(string filePath, bool machineOutput) {
    if (machineOutput) filePath = `"`~filePath~`"`;
    "filepath".tprintln(filePath);
}


private void printNetPath(string filepath) {
    if (filepath.endsWith(".desktop")) {
        "simple".tprint("URL: ");
        "netpath".tprintln(getURLFromDesktopFile(filepath));
    }
}


private void printDetails(string filepath, bool noColors) {
    SysTime sysTime;
    sysTime = amalthea.fs.timeLastModified(filepath);
    string lastModification = amalthea.sys.getTimeString(sysTime);
    sysTime = amalthea.fs.timeLastAccessed(filepath);
    string lastAccess = amalthea.sys.getTimeString(sysTime);
    
    string permissions = amalthea.sys.getUnixModeLine(filepath);
    auto size = std.file.getSize(filepath);
    string normSize = makeHumanOrientedByteSize(size);
    string typeOfOutputForProperty, typeOfOutputForValue;
    if (noColors) {
        typeOfOutputForProperty = typeOfOutputForValue = "simple";
    } else {
        typeOfOutputForProperty = "details_property";
        typeOfOutputForValue = "details_value";
    }
    typeOfOutputForProperty.tprint("Modified: "._s);
    typeOfOutputForValue.tprint(lastModification, "\t");
    typeOfOutputForProperty.tprint("Permissions: "._s);
    typeOfOutputForValue.tprintln(permissions);
    typeOfOutputForProperty.tprint("Accessed: "._s);
    typeOfOutputForValue.tprint(lastAccess, "\t");
    typeOfOutputForProperty.tprint("Size: "._s);
    typeOfOutputForValue.tprintln(normSize," (",size," ","B"._s,")");
}


private void printFragmentInfo(string filepath) {
    if (!filepath.endsWith(".fragpointer")) return;
    string typeOfOutputForProperty = "details_property";
    string typeOfOutputForValue = "details_value";
    auto info = getFragmentData(filepath);
    if (info.start.empty) info.start = "-";
    if (info.finish.empty) info.finish = "-";
    typeOfOutputForProperty.tprint("Type:     "._s);
    typeOfOutputForValue.tprintln(info.type);
    typeOfOutputForProperty.tprint("Start:    "._s);
    typeOfOutputForValue.tprintln(info.start);
    typeOfOutputForProperty.tprint("Finish:   "._s);
    typeOfOutputForValue.tprintln(info.finish);
    typeOfOutputForProperty.tprint("Original: "._s);
    typeOfOutputForValue.tprintln(info.original);
}


private auto getFragmentData(string filepath) {
    string content = readText(filepath);
    string[] lines = content.split("\n").array;
    string mediaType, original, start, finish;
    mediaType = lines[0].split("Type: ")[1].strip(' ');
    original  = lines[1].split("Path: ")[1].strip(' ');
    start     = lines[2].split("Start: ")[1].strip(' ');
    finish    = lines[3].split("Finish: ")[1].strip(' ');
    Tuple!(string, "type",  string, "original",
           string, "start", string, "finish") dataSet;
    dataSet = tuple(mediaType, original, start, finish);
    return dataSet;
}


private void printCategories(string filepath, bool noColors) {
    string[] categories = getCategoriesOfFile(filepath);
    foreach(i, c; categories) {
        string typeOfOutput = noColors ? "simple" : "category_details";
        typeOfOutput.tprint(`"`~c~`"`);
        if (categories.length-1 != i)
            typeOfOutput.tprint(" ");
        else
            "simple".tprint("\n");
    }
}




/*******************************************************************************
 * The function for command 'vitis show'
 */
deprecated void cmdShow(string[string] files, string[] options) {
    string numbers;
    bool paths, noColors, noNumbers, machine,
         reverse, flagCategories, details;
    SortEnum sortEnum = SortEnum.name; //default - sort by name
    options = "app" ~ options; //getopt requires application name
    try
    getopt(options, "n|numbers",  &numbers,
                    "sort",       &sortEnum,
                    "p|paths",    &paths,
                    "no-colors",  &noColors,
                    "no-numbers", &noNumbers,
                    "m|machine",  &machine,
                    "r|reverse",  &reverse,
                    "categories", &flagCategories,
                    "details",    &details);
    catch(ConvException e) {
        throw new ValidException("Error among command line flags."._s);
    }

    if (!stdout.isTTY) noColors = true;

    if (machine) noNumbers = noColors = true;
    
    setColorStdout(!noColors);
    setColorStderr(!noColors);

    bool extendedInformation;
    if (flagCategories || details) extendedInformation = true;
    
    string[] filepaths;
    //string[] linkpaths;
    string[] linknames;

    sortBySortEnum(sortEnum, files, linknames, filepaths);

    void subfn_printFileItem(string fileName,
                             ssize_t currNumber,
                             ssize_t maxNumber) {
        auto numberOfDigits = to!string(maxNumber).length;
        auto numberOfCurrentDigits = to!string(currNumber).length;
        string categoryPart;
        string namePart;

        if (!noNumbers) {
            auto numberOfSpaces = numberOfDigits-numberOfCurrentDigits;
            for(auto j = 0; j < numberOfSpaces; ++j) write(" ");
            "simple".tprint("[", currNumber, "] ");
        }
        ssize_t indexOfLastSlash;
        indexOfLastSlash = lastIndexOf(fileName, "/");
        
        if (indexOfLastSlash > 0) {
            categoryPart = fileName[0 .. indexOfLastSlash];
            if (machine) categoryPart = `"`~categoryPart;
            "category".tprint(categoryPart);
            "slash".tprint("/");
        }
        namePart = fileName[indexOfLastSlash+1 .. $];
        if (machine) namePart = paths ? namePart~`"` : `"`~namePart~`"`;
        "filename".tprint(namePart, "\n");
    }

    auto printableArray = paths ? filepaths : linknames;
    if (!flagHidden)
        printableArray = printableArray.filter!(a => !a.startsWith(".")).array;

    ssize_t maxNumber;
    if (numbers) {
        printableArray = getRarefiedArrayByNumbers(printableArray, numbers);
        foreach(i, f; printableArray) if ("" != f) maxNumber = i+1;
    } else {
        maxNumber = linknames.length;
    }

    if (paths) foreach(ref el; printableArray) {
        if (el.endsWith(".desktop")) {
            el = getURLFromDesktopFile(el);
        }
    }

    for(ssize_t index; index < printableArray.length; ++index) {
        ssize_t i;
        string f;
        if (!reverse) i = index; else i = printableArray.length -1 - index;
        f = printableArray[i];
        string fpath = filepaths[i];

        if (f == "") continue;
        subfn_printFileItem(f, i+1, maxNumber);
        if (!(flagCategories || details)) continue;
        if (details) {
            SysTime st;
            st = amalthea.fs.timeLastModified(fpath);
            string lastModification = amalthea.sys.getTimeString(st);
            st = amalthea.fs.timeLastAccessed(fpath);
            string lastAccess = amalthea.sys.getTimeString(st);
            string permissions = amalthea.sys.getUnixModeLine(fpath);
            ulong size = std.file.getSize(fpath);
            string normSize = makeHumanOrientedByteSize(size);
            string typeOfOutputForProperty, typeOfOutputForValue;
            if (noColors) {
                typeOfOutputForProperty = typeOfOutputForValue = "simple";
            } else {
                typeOfOutputForProperty = "details_property";
                typeOfOutputForValue = "details_value";
            }
            typeOfOutputForProperty.tprint("Modified: "._s);
                typeOfOutputForValue.tprint(lastModification, "\t");
            typeOfOutputForProperty.tprint("Permissions: "._s);
                typeOfOutputForValue.tprint(permissions, "\n");
            typeOfOutputForProperty.tprint("Accessed: "._s);
                typeOfOutputForValue.tprint(lastAccess, "\t");
            typeOfOutputForProperty.tprint("Size: "._s);
                typeOfOutputForValue.tprint(normSize," (",size," ","B"._s,")");
            "simple".tprint("\n");
        }
        if (flagCategories) {
            string[] currentCategories = getCategoriesOfFile(fpath);
            currentCategories = std.algorithm.sort(currentCategories).array;
            foreach(j, c; currentCategories) {
                string typeOfOutput = noColors ? "simple" : "category_details";
                typeOfOutput.tprint(`"`~c~`"`);
                if (currentCategories.length-1 != j) typeOfOutput.tprint(" ");
            }
            "simple".tprint("\n");
        }
        // if (printableArray.length-1 != i && (flagCategories || details))
        //     "simple".tprint("\n");
    } 
}


string makeHumanOrientedByteSize(ulong bytes) {
    long getExponent(ulong x) {
        if (x == 0) return 0;
        long exp = -1;
        while(x != 0) { x = x >> 10; ++exp; }
        return exp;
    }
    real size;
    string unit;
    auto exponent = getExponent(bytes);
    if (exponent > 5) exponent = 5;
    switch(exponent) {
        case 0: unit = "B"._s;   break;
        case 1: unit = "KiB"._s; break;
        case 2: unit = "MiB"._s; break;
        case 3: unit = "GiB"._s; break;
        case 4: unit = "TiB"._s; break;
        case 5: unit = "PiB"._s; break;
        default: break;
    }
    size = to!real(bytes)/(1024^^exponent);

    string strSize;
    
    if (exponent == 0) {
        strSize = to!string(size);
    } else {
        auto writer = appender!string();
        writer.formattedWrite("%.2f", size);
        strSize = writer.data;
    }

    return strSize ~ " " ~ unit;
}
unittest {
    assert(makeHumanOrientedByteSize(0) == "0 B");
    assert(makeHumanOrientedByteSize(1023) == "1023 B");
    assert(makeHumanOrientedByteSize(1024) == "1.00 KiB");
    assert(makeHumanOrientedByteSize(1025) == "1.00 KiB");
    assert(makeHumanOrientedByteSize(1024*1024*512) == "512.00 MiB");
    assert(makeHumanOrientedByteSize(1024L^^3*5/4) == "1.25 GiB");
    assert(makeHumanOrientedByteSize(1024L^^3*7/4) == "1.75 GiB");
}


/*******************************************************************************
 * The function for command 'vitis open'
 */
deprecated void cmdOpen(string[string] files, string[] options) {
    string numbers;
    bool savedPage;
    bool flagMix;
    bool reverse;
    string openingApp;
    SortEnum sortEnum = SortEnum.name; //default - sort by name
    options = "app" ~ options; //getopt requires application name
    try getopt(options, "n",          &numbers,
                        "sort",       &sortEnum,
                        "saved-page", &savedPage,
                        "app",        &openingApp,
                        "mix",        &flagMix,
                        "r|reverse",  &reverse);
    catch(ConvException e) {
        throw new ValidException("Error among command line flags."._s);
    }
    string[] filepaths, linknames;
    sortBySortEnum(sortEnum, files, linknames, filepaths);

    if (openingApp.empty) {
        openingApp = VitisConf.getOpener();
    }
    string openingCommand = openingApp;

    void subfn_fileIsApplication(string f) {
        string command = `xdg-mime query filetype `
                       ~ `"` ~ f.replace(`"`, `\"`) ~ `"`;
        string fileType = getAppOutput(command);
        if ("application/x-executable" == fileType) {
            string message =
                f ~ s_(": this file is application. "
                ~ "Perhaps it is not intended to be opened.");
            terminalOutput("warning", message, "\n");
        }
    }

    string[] resultArray;
    if (numbers) {
        resultArray = getRarefiedArrayByNumbers(filepaths, numbers);
    } else {
        resultArray = filepaths.dup;
    }

    if (reverse) {
        std.algorithm.reverse(resultArray);
    }

    if (flagMix) {
        import std.random : randomShuffle;
        randomShuffle(resultArray);
    }

    foreach(ref f; resultArray) {
        if ("" == f) continue;
        if (startsWith(f.baseName, ".") && !flagHidden) continue;
        f = f.replace(`"`, `\"`);
        if (endsWith(f, ".desktop")) {
            if (savedPage) {/*
                auto savedURL = getFieldFromDesktopFile(f, "URL[saved]");
                auto savedPagePath = getVitisHTMLPath ~ savedURL;
                if (!savedURL.empty && exists(savedPagePath)) {
                    f = savedPagePath;
                } else if (!savedURL.empty) {
                    "warning".tprint(savedPagePath,
                                     ": saved page is not found."._s, " ",
                                     "Skipped."._s, "\n");
                    continue;
                } else {
                    "warning".tprint("Saved page is not found."._s, " ",
                                     "Skipped."._s, "\n");
                    continue;
                }*/
            } else {
                f = getURLFromDesktopFile(f);
            }
        }
        subfn_fileIsApplication(f);
        openingCommand ~= " " ~ "\"" ~ f ~ "\"";
    }

    //writeln(openingCommand);
    c_system(openingCommand);
}


private string[] getCategoriesOfFile(string file) {
    string[] linkedCategories;
    string[] autoCategories;

    if (!file.exists) {
        "simple".tprint(file, ": file not found."._s, "\n");
        return linkedCategories;
    }

    if (file.isSymlink) file = file.readLink;
    else if (!file.startsWith('/')) file = pwd ~ "/" ~ file;

    string[string] allLinks = moduleVitisBase.getAllLinks();
    
    foreach(l, lf; allLinks) {
        if (lf != file) continue;
        string cat = l.dirName[getVitisPath.length .. $];
        if (cat.startsWith("__auto")) {
            // "__auto/N/NetworkBookmarsk" -> "NetworkBookmarks"
            import amalthea.dataprocessing : getIndex;
            auto indexOf1stSlash = getIndex(cat, '/'); 
            cat = cat[indexOf1stSlash+1 .. $];
            indexOf1stSlash = getIndex(cat, '/'); 
            cat = cat[indexOf1stSlash+1 .. $];
            autoCategories ~= cat;
            continue;
        }
        linkedCategories ~= cat;
    }
    linkedCategories = std.algorithm.sort(linkedCategories).array;
    autoCategories = std.algorithm.sort(autoCategories).array;
    return linkedCategories ~ autoCategories;
}


enum SortEnum {
    extension, name, none, size, time, atime
}


void sortBySortEnum(SortEnum sortEnum,
                    in  string[string] files,
                    out string[] filepaths,
                    out string[] linknames) {
    if (sortEnum != SortEnum.none) {
        //values of "files" are link names, keys are file paths
        orderMapByValues(files, filepaths, linknames);
    }
    final switch(sortEnum) {
        case SortEnum.extension:
            alias getExt = std.path.extension;
            filepaths.sort!((a,b) => getExt(a) < getExt(b));
            foreach(i, fpath; filepaths) linknames[i] = files[fpath];
            break;
        case SortEnum.name:
            filepaths.sort!((a,b) => files[a].baseName < files[b].baseName);
            foreach(i, fpath; filepaths) linknames[i] = files[fpath];
            break;
        case SortEnum.none: break;
        case SortEnum.size:
            filepaths.sort!((a,b) => std.file.getSize(a) < std.file.getSize(b));
            foreach(i, fpath; filepaths) linknames[i] = files[fpath];
            break;
        case SortEnum.time:
            alias lastModified = amalthea.fs.timeLastModified;
            filepaths.sort!((a, b) => lastModified(a) < lastModified(b));
            foreach(i, fpath; filepaths) linknames[i] = files[fpath];
            break;
        case SortEnum.atime:
            import std.datetime;
            alias lastAccessed = amalthea.fs.timeLastAccessed;
            filepaths.sort!((a, b) => lastAccessed(a) < lastAccessed(b));
            foreach(i, fpath; filepaths) linknames[i] = files[fpath];
            break;
    }
}
