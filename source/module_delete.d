/* This file is part of Vitis.
 *
 * Copyright (C) 201-2019 Eugene 'Vindex' Stulin
 *
 * Vitis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import module_vitis_base;
import module_help;
public alias moduleDelete = module_delete;


/*******************************************************************************
 * Primary function for removing categories
 * and for alienating categories from files
 */
void mainFn(string[] args) {
    string err = "Wrong using of command 'delete'."._s;
    checkCondition!err(args.length >= 2);
    if (args[1] =="--help" || args[1] == "-h") {
        checkCondition!err(args.length == 2);
        fnHelp("delete");
        return;
    }
    if (!args[1].among("-c", "-v", "-f")) {
        args = args[0] ~ ["-c"] ~ args[1 .. $];
    }

    string[] categories = extractOptionRange(args, "-c");
    size_t number = 0;
    args.getopt(config.passThrough, "number", &number);

    if (!categories.empty) {
        string[] filenames = extractOptionRange(args, "-f");
        if (filenames.empty) {
            bool force = false;
            args.getopt(config.passThrough, "force", &force);
            checkCondition!err(args.length == 1);
            deleteCategories(categories, force); 
        } else {
            checkCondition!err(args.length == 1 && categories.length == 1);
            checkCondition!err(filenames.length == 1
                            || filenames.length > 1 && number == 0);
            string category = categories[0];
            foreach(f; filenames) {
                deleteCategoryFromFile(category, f, number);
            }
        }
    } else {
        string[] vitisFiles = extractOptionRange(args, "-v");
        checkCondition!err(args.length == 1);
        foreach(vitisFile; vitisFiles) {
            deleteFileInVitis(vitisFile, number);
        }
    }
    
}


void deleteCategories(string[] categories, bool force = false) {
    static void deleteAliasesOfCategory(string category) {
        auto categoryPath = getCatDirPath(category);
        auto categories = getAliases(Yes.fullPath);
        foreach(cat; categories) {
            if (categoryPath == readLink(cat)) {
                rm(cat);
            }
        }
    }
    foreach(category; categories) {
        if (category.isAutocategoryName) {
            string msg;
            msg = category ~ ": deletion of this category is prohibited."._s;
            "warning".tprintln(msg, " ", "Skipped."._s);
            continue;
        }
        auto categoryPath = getCatDirPath(category).stripRight('/');
        if ("" == categoryPath) {
            "warning".tprint(category, ": this category doesn't exist."._s,
                             " ", "The request is skipped."._s, "\n");
            continue;
        }
        if (categoryPath.isSymlink) {
            if (!force) {
                rm(categoryPath);
                return;
            }
            category = getCategoryByAlias(category);
            categoryPath = getCatDirPath(category);
        }
        deleteAliasesOfCategory(category);
        rmdirRecurse(categoryPath);
    }
}


void deleteCategoryFromFile(string category,
                            string filename,
                            size_t number = 0) {
    string vitisFile = category ~ "/" ~ filename;
    string linkpath;
    try {
        linkpath = moduleVitisBase.findFileInVitis(vitisFile, number)[1];
    } catch(FileWarning e) {
        "warning".tprintln(e.msg, " ", "The request is skipped."._s);
        return;
    }
    rm(linkpath);
}


void deleteFileInVitis(string vitisFile,
                       size_t number = 0) {
    string category = std.path.dirName(vitisFile);
    if (category == ".") category = "";
    string filename = std.path.baseName(vitisFile);

    string[string] allLinks = getAllLinks();
    size_t counter;
    if (category.empty) {
        if (number > 1) {
            "error".tprint("A non-existing number is specified."._s);
            return;
        }
        foreach(pathToLink, pathToFile; allLinks) {
            if (baseName(pathToLink) == filename) {
                rm(pathToLink);
                counter++;
            }
        }
    } else {
        string pathToFileForRemoval = findFileInVitis(vitisFile, number)[0];
        foreach(pathToLink, pathToFile; allLinks) {
            if (pathToFileForRemoval == pathToFile) {
                rm(pathToLink);
                counter++;
            }
        }
    }
    if (counter == 0) {
        throw new FileWarning(vitisFile ~ ": file in Vitis is not found."._s);
    }
}
