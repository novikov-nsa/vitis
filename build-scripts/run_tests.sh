#!/bin/bash -e
. build-scripts/env.sh

if [[ ! -e testing/testing.sh ]]; then
    echo "No tests"
    exit
fi

if [[ ! -e ${BINPATH} ]]; then
    echo "Binary executable file doesn't exist."
    echo "Run 'make && make test'"
    exit
fi

cp ${BINPATH} testing/
cd testing
    ./testing.sh
cd -
rm testing/${APP}
