#!/bin/bash -e
source build-scripts/env.sh
set -x

BASE_URL=https://bitbucket.org/vindexbit
PKG_NAME=""
LINK_DESCRIPTION=""

makePackageLinkInREADME() {
    PKG_URL=${BASE_URL}/${APP}/downloads/${PKG_NAME}
    MD_LINK="[${PKG_NAME}](${PKG_URL})"
    MD_PKG_LINE="${LINK_DESCRIPTION} ${MD_LINK}"

    MD_PKG_LINE_REGEX=$(
        echo $MD_PKG_LINE   |
        sed "s/\//\\\\\//g" |
        sed "s/\[/\\\\[/g"  |
        sed "s/\]/\\\\]/g"  |
        sed "s/(/\\\\(/g"   |
        sed "s/)/\\\\)/g"
    )

    sed "s/^${LINK_DESCRIPTION}.*/${MD_PKG_LINE_REGEX}/g" README.md > tmp
    cat tmp > README.md
    rm tmp
}

PKG_NAME=${APP}_${VERSION}_amd64.deb
LINK_DESCRIPTION="Latest version of deb-package:"
makePackageLinkInREADME


PKG_NAME=${APP}-${VERSION}.tar.xz
LINK_DESCRIPTION="Latest version of tarball with sources:"
makePackageLinkInREADME

#PKG_NAME=${APP}_${VERSION}_armhf.deb
#LINK_DESCRIPTION="Latest version of deb-package for ARMv7:"
#makePackageLinkInREADME

set +x
