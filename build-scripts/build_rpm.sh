#!/bin/bash -e
source build-scripts/env.sh
set -x

PLANG=$1
if [[ $PLANG == D ]]; then
    DC=$2
    PHOBOS_LINKING=$3
    if [[ $DC == ldc2 ]]; then
        BUILDREQ="BuildRequires:  ldc"
    elif [[ $DC == dmd ]]; then
        BUILDREQ="BuildRequires:  dmd"
    fi
fi

RPMINFO=pkg-info/rpm-info
SPECFILE=build/rpmbuild/SPECS/${APP}.spec

NAMELINE="Name:           ${APP}"
VERSLINE="Version:        ${VERSION}"
SUMMLINE="Summary:        ${SUMMARY:0:-1}" # delete the last dot
HOMEPAGE_REGEX=`sed -e 's/\//\\\\\//g' pkg-info/homepage`
PAGELINE="URL:            ${HOMEPAGE_REGEX}"

mkdir -p build/rpmbuild
cd build/rpmbuild
    mkdir -p BUILD BUILDROOT RPMS SOURCES SPECS SRPMS
cd -

make_spec() {
    sed "s/^Name:.*/${NAMELINE}/g" ${RPMINFO}/template.spec > tmp1
    sed "s/^Version:.*/${VERSLINE}/g"          tmp1 > tmp2
    sed "s/^Summary:.*/${SUMMLINE}/g"          tmp2 > tmp3
    sed "s/^URL:.*/${PAGELINE}/g"              tmp3 > tmp4
    sed "s/^BuildRequires:.*/${BUILDREQ}/g"    tmp4 > tmp5
    sed '/%description/r pkg-info/description' tmp5 > tmp6
    sed '/%changelog/r   changelog'   tmp6 > ${SPECFILE}
    rm tmp*
}

make_rpm() {
    if [[ -e ~/.rpmmacros ]] ; then
        cp ~/.rpmmacros ~/.rpmmacros.original
    else
        touch ~/.rpmmacros
    fi
    echo "%_topdir ${PWD}/build/rpmbuild" >> ~/.rpmmacros
    cp build/${APP}-${VERSION}.tar.xz build/rpmbuild/SOURCES
    rpmbuild -ba build/rpmbuild/SPECS/${APP}.spec
    rm ~/.rpmmacros
    if [[ -e ~/.rpmmacros.original ]] ; then
        mv ~/.rpmmacros.original ~/.rpmmacros
    fi
    rpmlint build/rpmbuild/RPMS/`uname -m`/*.rpm
}

make_spec
make_rpm

set +x
