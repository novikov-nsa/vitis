#!/bin/bash -e
source build-scripts/env.sh
set -x

ALL="changelog COPYING copyright Makefile README.md \
build-scripts/ help/ pkg-info/ source/ testing/ resources/ doc/"
PKG="${APP}-${VERSION}"

mkdir -p ${PKG} build/
cp -r ${ALL} ${PKG}
tar --xz -cf build/${PKG}.tar.xz ${PKG}
rm -rf ${PKG}

set +x
