#!/bin/bash -e
source build-scripts/env.sh
set -x

PKG=${APP}

PLANG=$1 # Programming Language (Python or D)
if [[ $PLANG == D ]]; then
    DEPENDS="libc6 (>= 2.14)"
    DC=$2
    PHOBOS_LINKING=$3
    if [[ $DC == ldc2 && $PHOBOS_LINKING == dynamic ]]; then
        DEPENDS+=", libphobos2-ldc-shared82 (>= 1:1.12.0-1)"
    elif [[ $DC == gdc && $PHOBOS_LINKING == dynamic ]]; then
        DEPENDS+=", libgphobos76 (>= 8.3.0-2)"
    fi
fi

DEBINFO=pkg-info/deb-info
VERSLINE="Version: ${VERSION}"
ARCHLINE="Architecture: ${ARCH}"
DEPENDS_LINE="Depends: ${DEPENDS}" 
MAINTAINER="Maintainer: `cat pkg-info/maintainer`"
HOMEPAGE_REGEX=`sed -e 's/\//\\\\\//g' pkg-info/homepage`
HOMEPAGELINE="Homepage: ${HOMEPAGE_REGEX}"
DESCRIPTION_BEGIN="Description: ${SUMMARY,}"

make_control() {
    sed "s/^Package:.*/Package: ${PKG}/g" "${DEBINFO}/template.control" > tmp1
    sed "s/^Version:.*/${VERSLINE}/g"              tmp1 > tmp2
    sed "s/^Architecture:.*/${ARCHLINE}/g"         tmp2 > tmp3
    sed "s/^Depends:.*/${DEPENDS_LINE}/g"          tmp3 > tmp4
    sed "s/^Homepage:.*/${HOMEPAGELINE}/g"         tmp4 > tmp5
    sed "s/^Maintainer:.*/${MAINTAINER}/g"         tmp5 > tmp6
    sed "s/^Description:.*/${DESCRIPTION_BEGIN}/g" tmp6 > tmp7
    sed "/Description/r pkg-info/description"      tmp7 > tmp8
    sed "/^${APP}/,/\$./s/^/ /"                    tmp8 > ${DEBINFO}/control
    rm tmp*
}

DEBPATH=build/deb/${ARCH} # temporary place to build
DEB_BASHCOMP=${DEBPATH}/usr/${RL_BASHCOMP}
DEB_HELPDIR_EN=${DEBPATH}/usr/${RL_HELPDIR_EN}
DEB_HELPDIR_RU=${DEBPATH}/usr/${RL_HELPDIR_RU}
DEB_HELPDIR_EO=${DEBPATH}/usr/${RL_HELPDIR_EO}
DEB_MANDIR_EN=${DEBPATH}/usr/${RL_MANDIR_EN}
DEB_MANDIR_RU=${DEBPATH}/usr/${RL_MANDIR_RU}
DEB_MANDIR_EO=${DEBPATH}/usr/${RL_MANDIR_EO}

make_deb() {
    mkdir -p ${DEBPATH}/DEBIAN
    mkdir -p ${DEBPATH}/usr/bin
    cp ${BINPATH} ${DEBPATH}/usr/bin/${APP}
    if [[ -e source/_${APP} ]] ; then
        mkdir -p ${DEB_BASHCOMP}
        cp source/_${APP} ${DEB_BASHCOMP}/${APP}
    fi

    #help-pages
    mkdir -p ${DEB_HELPDIR_EN}
    mkdir -p ${DEB_HELPDIR_RU}
    mkdir -p ${DEB_HELPDIR_EO}
    cp help/en_US/help*.txt ${DEB_HELPDIR_EN}/
    cp help/ru_RU/help*.txt ${DEB_HELPDIR_RU}/
    cp help/eo/help*.txt    ${DEB_HELPDIR_EO}/

    #copyright and control
    mkdir -p ${DEBPATH}/usr/${RL_DOCDIR_SPEC}
    cp copyright ${DEBPATH}/usr/${RL_DOCDIR_SPEC}
    chmod 644 ${DEBPATH}/usr/${RL_DOCDIR_SPEC}/copyright
    mv ${DEBINFO}/control ${DEBPATH}/DEBIAN/
    chmod 644 ${DEBPATH}/DEBIAN/control

    #permissions
    chmod 755 ${DEBPATH}/usr/
    chmod 755 ${DEBPATH}/usr/bin
    chmod 755 ${DEBPATH}/usr/bin/${APP}
    chmod 755 ${DEBPATH}/usr/share/
    if [[ -e source/_${APP} ]] ; then
        chmod 755 ${DEBPATH}/usr/share/bash-completion
        chmod 755 ${DEBPATH}/usr/share/bash-completion/completions
        chmod 755 ${DEBPATH}/usr/share/bash-completion/completions/${APP}
    fi
    chmod 755 ${DEBPATH}/usr/share/doc
    chmod 755 ${DEBPATH}/usr/share/doc/${APP}
    chmod 755 ${DEBPATH}/usr/share/help
    chmod 755 ${DEBPATH}/usr/share/help/en_US
    chmod 755 ${DEBPATH}/usr/share/help/ru_RU
    chmod 755 ${DEBPATH}/usr/share/help/eo
    chmod 755 ${DEBPATH}/usr/share/help/en_US/${APP}
    chmod 644 ${DEBPATH}/usr/share/help/en_US/${APP}/*.txt
    chmod 755 ${DEBPATH}/usr/share/help/ru_RU/${APP}
    chmod 644 ${DEBPATH}/usr/share/help/ru_RU/${APP}/*.txt
    chmod 755 ${DEBPATH}/usr/share/help/eo/${APP}
    chmod 644 ${DEBPATH}/usr/share/help/eo/${APP}/*.txt

    #changelog and md5sums
    gzip -9 -k -n changelog
    mv changelog.gz ${DEBPATH}/usr/${RL_DOCDIR_SPEC}/
    cd ${DEBPATH}
        find usr -type f -exec md5sum {} + > DEBIAN/md5sums
        chmod 644 DEBIAN/md5sums
    cd -

    #generating
    fakeroot dpkg-deb --build ${DEBPATH} build/deb
    #if [[ -e /usr/bin/lintian ]] ; then
        #lintian build/deb/*.deb
    #fi
}

make_control
make_deb


set +x

