#!/bin/bash -e
source build-scripts/env.sh
set -x

MODE="$1"
PREFIX="$2"

if [[ ${PREFIX} != "" ]] ; then
    BASE="${PREFIX}"
else
    BASE=${DEFAULT_ALTROOTDIR}
fi

ALTBINDIR="${BASE}/${RL_BINDIR}"
ALTSHAREDIR="${BASE}/${RL_SHAREDIR}"
ALTDOCDIR="${BASE}/${RL_DOCDIR}"
ALTMANDIR_EN="${BASE}/${RL_MANDIR_EN}"
ALTMANDIR_RU="${BASE}/${RL_MANDIR_RU}"
ALTMANDIR_EO="${BASE}/${RL_MANDIR_EO}"
ALTBASHCOMPDIR="${BASE}/${RL_BASHCOMP}"
ALTHELPDIR="${BASE}/${RL_HELPDIR}"

ALTDOCDIR_SPEC="${BASE}/${RL_DOCDIR_SPEC}"
ALTHELPDIR_EN="${BASE}/${RL_HELPDIR_EN}"
ALTHELPDIR_RU="${BASE}/${RL_HELPDIR_RU}"
ALTHELPDIR_EO="${BASE}/${RL_HELPDIR_EO}"


echo "prefix:" ${BASE}
if [[ ${MODE} == "--install" ]] ; then
    mkdir -p "${ALTBINDIR}" "${ALTDOCDIR_SPEC}"
    install "${BINPATH}" "${ALTBINDIR}/"
    if [[ -e source/_${APP} ]] ; then
        mkdir -p "${ALTBASHCOMPDIR}"
        cp source/_${APP} "${ALTBASHCOMPDIR}/${APP}"
    fi
    cp copyright ${ALTDOCDIR_SPEC}/
    mkdir -p ${ALTHELPDIR_EN} ${ALTHELPDIR_RU} ${ALTHELPDIR_EO}
    install help/en_US/help*.txt ${ALTHELPDIR_EN}/
    install help/ru_RU/help*.txt ${ALTHELPDIR_RU}/
    install help/eo/help*.txt    ${ALTHELPDIR_EO}/
    #mkdir -p ${ALTMANDIR_EN} ${ALTMANDIR_RU} ${ALTMANDIR_EO}
    #gzip -9 -k -n help/${APP}.1
    #gzip -9 -k -n help/${APP}.ru.1
    #gzip -9 -k -n help/${APP}.eo.1
    #install help/${APP}.1.gz    ${ALTMANDIR_EN}/${APP}.1.gz
    #install help/${APP}.ru.1.gz ${ALTMANDIR_RU}/${APP}.1.gz
    #install help/${APP}.eo.1.gz ${ALTMANDIR_EO}/${APP}.1.gz
    #rm help/*.1.gz
else
    rm -f ${ALTBINDIR}/${APP}
    rm -f ${ALTBASHCOMPDIR}/${APP}
    rm -rf ${ALTDOCDIR_SPEC}
    rm -rf ${ALTHELPDIR_EN}
    rm -rf ${ALTHELPDIR_RU}
    rm -rf ${ALTHELPDIR_EO}
    #rm -f ${ALTMANDIR_EN}/${APP}.1.gz
    #rm -f ${ALTMANDIR_RU}/${APP}.1.gz
    #rm -f ${ALTMANDIR_EO}/${APP}.1.gz
fi


set +x
