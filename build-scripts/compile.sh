#!/bin/bash -e
source build-scripts/env.sh
SRCDIR=source/
SRC="${SRCDIR}/*.d"

DEB_HOST_MULTIARCH=`dpkg-architecture -qDEB_HOST_MULTIARCH`

BINVERSION=$1     # release or debug
DC=$2             # dmd, ldc2
PHOBOS_LINKING=$3 # dynamic or static
AMALTHEA_LINKING=static
STATIC_OPTIONS=""

BINPATH="${BINPATH_BASEDIR}/${APP}"

if [[ ${PHOBOS_LINKING} == static ]]; then
    if [[ ${DC} == ldc2 ]] ; then
        STATIC_OPTIONS=-defaultlib=:libphobos2-ldc.a,:libdruntime-ldc.a
        LDC_VERSION=`ldc2 --version | head -n 1 \
            | sed "s/LDC - the LLVM D compiler (//" | sed "s/)://"`
        echo Version of LLVM D Compiler: ${LDC_VERSION}
        if [[ "${LDC_VERSION}" != "1.2.0" ]]
        then
            STATIC_OPTIONS+=" -link-defaultlib-shared=false -L-lz"
        fi
    elif [[ ${DC} == gdc ]]; then
        STATIC_OPTIONS=-static-libphobos # unfortunately, no effects
    fi
fi
if [[ ${AMALTHEA_LINKING} == dynamic ]]; then
    if [[ ${DC} == gdc ]]; then
        LIBS="-lamalthea-${DC}"
    else
        LIBS="-L-L/usr/lib/${DEB_HOST_MULTIARCH}/ -L-l:libamalthea-${DC}.so"
    fi
else #static
    if [[ ${DC} == gdc ]]; then
        LIBS="-lamalthea-${DC}" # unfortunately, here is a problem
    else
        LIBS="-L-L/usr/lib/${DEB_HOST_MULTIARCH}/ -L-l:libamalthea-${DC}.a"
    fi
fi

mkdir -p ${BINPATH_BASEDIR}


if [[ ${DC} == dmd ]] ; then
    DEBUG_OPTIONS="-debug -g"
elif [[ ${DC} == ldc2 ]] ; then
    DEBUG_OPTIONS="-d-debug -gc"
elif [[ ${DC} == gdc ]] ; then
    DEBUG_OPTIONS="-fdebug"
fi

OUT="-of"
OPTIM="-O -release"
if [[ ${DC} == gdc ]] ; then
    OUT="-o "
    OPTIM="-O2 -frelease"
fi


set -x

LOCALES_CSV=resources/locales.csv
LOCALES_DTXT=source/locales.dtxt
if [[ -e /usr/bin/rdmd ]]; then
    ${BS}/localesconverter.script.d "${LOCALES_CSV}" "${LOCALES_DTXT}"
else
    ${DC} ${BS}/localesconverter.script.d ${OUT}localesconverter
    ./localesconverter "${LOCALES_CSV}" "${LOCALES_DTXT}"
    rm localesconverter
fi


if [[ $BINVERSION == release ]] ; then
    OPTS="${OPTIM} ${STATIC_OPTIONS} ${LIBS} -Jsource/"
    ${DC} ${SRC} ${OUT}${BINPATH} ${OPTS}
    objcopy --strip-debug --strip-unneeded ${BINPATH} ${BINPATH}
else
    OPTS="${DEBUG_OPTIONS} ${STATIC_OPTIONS} ${LIBS} -Jsource/"
    ${DC} ${SRC} ${OUT}${BINPATH} ${OPTS}
fi

cd "${BINPATH_BASEDIR}"
    chrpath -d ${APP}
    chmod 755 ${APP}
cd -

set +x
