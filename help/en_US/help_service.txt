Basic format for command 'service':
    vitis service <subcommand> <data> [options]

The command is used to configure vitis and retrieve service information.
By default, the settings are stored in the file '~/.vitis.conf', but each time
you call vitis, you can use an alternate configuration file
using option '--conf=<path_to_configure_file>'.

Subcommands 'add', 'get', 'set' allow to work with the vitis settings.
The configuration elements currently are:
    path               - path where vitis stores categories and links to files;
    lang               - used language (auto, en_US, eo, ru_RU);
    filespace(s)       - paths to directories of file spaces in the file system;
    autosave           - automatic save to file space;
    opener             - program selected for universal opening of files;
    casesensitivity    - case sensitivity for categories;
    autocategorization - mode of automatic categories;
    default            - parameters by default (only for the set command).

    vitis service set path <my_path>
        Sets the path to the root directory of Vitis.
        Example:
            vitis service set path /data/local/VitisDir

    vitis service get path
        Displays the path to the root directory of Vitis.
        By default, '/home/$USERNAME/Vitis'.

    vitis service set lang <language>
        Sets the language localization for vitis.
        Example:
            vitis service set lang ru_RU
        Supports 'en_US', 'ru_RU' and 'eo'. The 'auto' value is available,
        at which the language is determined by the system settings.

    vitis service get lang
        Displays the language locale used, for example, ru_RU or en_US.
    
    vitis service set autosave <value>
        The value can be "yes" or "no".
        With "yes", a mode is set in which new files are automatically copied
        to the first available file space.
    
    vitis service get autosave
        Displays the value of the autosave option.

    vitis service set opener <application>
        The user-specified program becomes a universal program
        for opening files.
        Example:
            vitis service set opener mime-open

    vitis service get opener
        Displays the program selected for use with the 'vitis open' command.
        By default, the vts-fs-open utility is used (by-product of the Vitis
        project).

    vitis service set casesensitivity <value>
        Sets case sensitivity of categories.
        The value can be "yes" or "no".
        Example:
            vitis service set casesensitivity no

    vitis service get casesensitivity
        Indicates whether vitis is sensitive to the register
        in relation to categories.
        Outputs "yes" or "no".
    
    vitis service set autocategorization <value>
        Sets the autocategorization mode.
        Valid values ​​are 'format', 'extension' and 'none'.
        If 'format' is set, files added to Vitis are automatically assigned
        categories indicating file formats.
        If 'extensionl is set, the files correspond to the categories
        denoting file extensions.
        Values ​​can be combined with a semicolon.
        By default, the autocategorization mode is set to 'format;extension'.
        The value 'none' disables autocategories.
    
    vitis service get autocategorization
        Shows the current autocategorization mode.

    vitis service set default
        Resets settings to default values.

The configuration parameters 'filespace' and 'filespaces' allows to assign
files to which the Vitis system will refer.
File spaces are used in the 'assign' command with the '--save' flag:
    vitis service reset filespaces
    vitis service add filespace /mnt/BigDisk/All
    vitis assign -c Photo -f /my/path/image.jpg --save
        The file 'image.jpg' from '/my/path/' will be copied
        to the first available directory from the number of filespaces
        (i.e. to the directory '/mnt/BigDisk/All').
        Details:
            vitis assign --help

    vitis service get filespaces
        Displays a list of paths to file spaces.
        By default, the list is empty.
    
    vitis service reset filespaces
        Clears the list of file spaces.

    vitis service add filespace </my/path>
        Allows to add a file space.
        Example:
            vitis service add filespace /mnt/BigDisk/All/

    vitis service prioritize filespace </my/path>
        Puts the file space first in the list.

vitis service disregard filespace </path/to/filespace>
    Removes unnecessary file space from the configuration settings.
    Example:
        vitis service disregard filespace /mnt/BigDisk/All

Subcommand 'check' checks the state of the Vitis system.
Format of subcommand:
    vitis check <object> [-c category] [[--fix][--interactive]]
Objects of checking (options after 'check'):
    broken-links      - broken symbolic links;
    saved-pages       - presence of saved pages to network bookmarks;
    fileformats       - checking that all files have the correct automatic
                        categories indicating file formats.

Option '--fix' fixes invalid situations.
Option '-c' allows to specify the category by which the check will be performed.
    If you do not use the '-c' option,
    the test is performed throughout the Vitis system.
Option '--interactive' is a compromise between simple diagnostics
    and automatic fixing of problems. User independently makes a decision
    on each case during the diagnostics.
    It cancels the '--fix' option.
Option '--fresh' is only available for option 'saved-pages',
    it allows to update local copies of previously save pages.

Examples:

    vitis check broken-links
        It will search for the broken links, the result of the diagnostics
        will be displayed in the terminal for the possibility of correction.

    vitis check broken-links --fix
        All broken links will be deleted.

    vitis check broken-links -c Музыка --interactive
        Diagnostics for the presence of broken links in the category 'Music'.
        For each such link the user is asked to delete.

    vitis check saved-pages
        Passes through the contents of the automatic category
        'NetworkBookmarks',
        displays information about which pages were not saved locally.

    vitis check saved-pages --fix
        Local copies of the pages will be saved
        for all bookmarks those did not have them.

    vitis check saved-pages --fresh
        Previously created local copies will be updated.

    vitis check saved-pages --fix --fresh
        All net-bookmarks will receive updated copies of the pages.

    vitis check fileformats -c "Computer Science"
        A check is made for the exposure and correctness of automatic categories
        denoting file formats for the category "Computer Science".
    
