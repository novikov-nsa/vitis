Formato por komando 'open':
    vitis open [-e] <dateno> [opcioj]

Malfermo de dosieroj laŭ esprimo:
    vitis open [-e] <esprimo> [opcioj] ...
    
Malfermo okazas per defaŭltaj programoj. Se programo "vts-fs-open" estas
instalita, ĝi determinas defaŭltajn programojn laŭ formatoj (se agordoj
ne estis modifitaj).

Per flago "-n" oni elektas necesajn dosierojn laŭ numeroj.
Numerojn oni nomas tra komo kaj kiel intervaloj kun streketo (sen spacetoj).

Flago --sort=<ranĝado> fiksas reĝimon de ranĝado:
      --sort=extension - laŭ dosiera sufikso;
      --sort=name      - laŭ alfabeta ranĝo;
      --sort=none      - sen ranĝado;
      --sort=size      - laŭ grando;
      --sort=time      - laŭ tempo de modifo;
      --sort=atime     - laŭ tempo de atingo.

Flago "--hidden" ekfunkciigas elirigon de kaĉaj dosieroj
    kaj kaĉaj kategorioj (kun "--categories").
    
Flago "--app" estas uzata por elekti alternativan programon por malfermo de
dosieroj. Ekzemple:
    vitis open Muziko --app qmmp
        Dosieroj kun kategorio "Muziko" malfermos per QMMP.

Flago "--saved-page" estas uzata por malfermi lokajn kopiojn de konservitaj
HTML-paĝoj (se ekzistas) anstataŭ ligiloj al originalajn fontojn.

Flago "--mix" miksas liston de dosieroj.

Flago "--reverse" inversas liston de dosieroj.


Malfermo laŭ nomo el Vitis-sistemo:
    vitis open -v <kategorio/dosiero> [opcioj] ...

Ĉi tiu komando donas pli simplan kaj ĝustan procedon de malfermo de dosiero -
laŭ ĝia nomo kaj kategorio.

Ĉi tie oni povas uzi flagoin "--app", "--saved-page".

Se kategorio estas indikita, por ĝustigo de numero de dosiero
el samnomaj dosieroj vi povas uzi "--number".
