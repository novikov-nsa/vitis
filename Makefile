APP=vitis
BS=build-scripts
PREFIX=/usr/local
DESTDIR=
ALL=changelog copyright COPYING Makefile README.md \
source/ help/ pkg-info/ $(BS)/ testing/ resources/ doc/

DEFAULT_PLANG=D
PLANG=$(DEFAULT_PLANG)
MULTILINGUAL=FALSE
ifeq ($(MULTILINGUAL),TRUE)
	PLANG_DIRNAME=lang_$(PLANG)
else
	PLANG_DIRNAME=
endif

ifeq ($(PLANG),D)
	DC=ldc2 #dmd, gdc or ldc2
	PHOBOS_LINKING=static #static or dynamic
	RELEASE_ARGS=release $(DC) $(PHOBOS_LINKING)
	DEBUG_ARGS=debug $(DC) static
	SPEC_PKG_OPTIONS=$(PLANG) $(DC) $(PHOBOS_LINKING)
else
	SPEC_PKG_OPTIONS=$(PLANG)
endif

bin:
	$(BS)/$(PLANG_DIRNAME)/compile.sh $(RELEASE_ARGS)

debug:
	$(BS)/$(PLANG_DIRNAME)/compile.sh $(DEBUG_ARGS)

test:
	$(BS)/run_tests.sh

doc: doc/req-ru/requirements.tex
	cd doc/req-ru/; pdflatex requirements.tex
	cd doc/req-ru/; pdflatex requirements.tex
	cd doc/req-ru/; rm -f *aux *log *out *toc

update_readme:
	$(BS)/update_readme.sh

tarball: $(ALL) clean update_readme
	$(BS)/build_tarball.sh

install:
	$(BS)/install.sh --install $(DESTDIR)$(PREFIX)

uninstall:
	$(BS)/install.sh --uninstall $(DESTDIR)$(PREFIX)

deb: debug update_readme
	$(BS)/build_deb.sh $(SPEC_PKG_OPTIONS)

install-deb: deb
	sudo dpkg -i build/deb/*.deb

rpm: bin update_readme tarball
	$(BS)/build_rpm.sh $(SPEC_PKG_OPTIONS)

install-rpm: rpm
	sudo rpm --reinstall build/rpmbuild/RPMS/`uname -m`/*.rpm

release: tarball deb rpm

clean:
	rm -rf build/ doc/req-ru/requirements.pdf
