Vitis is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

---

## vitis

vitis is a category manager for files.
It allows to move from a hierarchical organization of files to a semantic one.
Categories are stored as directories (in ~/Vitis by default).
These directories contain links to file on your file system.
The vitis settings are stored in the file '~/.config/vitis/vitis.conf'.

See details: `vitis --help`

---

## Ready-made packages

Latest version of tarball with sources: [vitis-0.10.0.tar.xz](https://bitbucket.org/vindexbit/vitis/downloads/vitis-0.10.0.tar.xz)

### x86_64

Latest version of deb-package: [vitis_0.10.0_amd64.deb](https://bitbucket.org/vindexbit/vitis/downloads/vitis_0.10.0_amd64.deb)


*Checked on Debian 10.*

---

## Build from source

### Preparing

Before assembling, you need to install a compiler for D (this project supports compilers [dmd](https://dlang.org/download.html#dmd), [gdc](https://gdcproject.org/) and [ldc](https://github.com/ldc-developers/ldc/)) and [chrpath](https://directory.fsf.org/wiki/Chrpath).

For example, in Debian-based distributions, you can install required packages as follows:

`sudo apt install ldc chrpath`

Similarly, in Fedora:

`sudo dnf install ldc chrpath`

This project is assembled with a static linking to the [Amalthea library](https://gitlab.com/tech.vindex/amalthea). So if you want to build this project from source, you also need to build and install Amalthea. Then return to this instruction.


### Compilation and installation


Creating of executable bin-file:

`make`

Also, you can choose a compiler for assembling:

`make DC=dmd`

Installation (by default, main directory is /usr/local/):

`sudo make install`

After that, the application is ready for use.

You can install this application in any other directory:

`make install PREFIX=/home/$USER/sandbox`

Uninstall:

`sudo make uninstall`

If you installed in an alternate directory:

`make uninstall PREFIX=/home/$USER/sandbox`


---

## Start

First, create your first category:

`vitis create Music`

Now assign this category to a file:

`vitis assign Music -f "/home/$USER/Downloads/Josh Woodward - Swansong.ogg"`

The quotation marks are used when the path to the file contains spaces.

Command 'show' can show you all the files from the category:

`vitis show Music`

You can view the contents of the category with all the details about the files:

`vitis show Music --paths --details --categories`

You can open files by default application<br>(for best result, it's recommended to install [vts-fs-open](https://bitbucket.org/vindexbit/vts-fs-open)):

`vitis open Music`

If you want to eliminate the category from the file, use 'delete':

`vitis delete Music -f "Josh Woodward - Swansong.ogg"`

The main advantages of this program are the ability to assign multiple categories to one file and use mathematical expressions with operations on sets to select files.

Example:

`vitis assign Music 2010s -f "/home/$USER/Downloads/Josh Woodward - Swansong.ogg"`

`vitis open Music i: 2010s`

*Note: **'i:'** is intersection*.

---

**vitis** has a large number of different functions, see the details in the help:

`vitis --help`

---

## Donate:

* [Bank cards / Yandex.Money Wallets](https://money.yandex.ru/to/410012626193056)

* [Bank cards / Qiwi](https://my.qiwi.com/form/Evgenyi-SUTTDFH0sl)

* [PayPal](https://www.paypal.me/evstulin)

* ETH: `0xA65DEE0Cd4Cd7ef05583a07a4303e0C5B306eA7B`

* BTC: `13uF1MMxWGDktpe8f19rxoDNYkaZ6aoiuU`

 * [Liberapay.com](https://liberapay.com/Vindex)

---

## Feedback

Questions, suggestions, comments, bugs:

**tech.vindex@gmail.com**

Also use the repository service tools.

---