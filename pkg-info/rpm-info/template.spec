Name:           
Version:        
Summary:        
License:        GPLv3
URL:            

Release:        1%{?dist}
Source0:        https://bitbucket.org/vindexbit/%{name}/downloads/%{name}-%{version}.tar.xz
BuildRequires:  

%description

%global debug_package %{nil}

%prep
%autosetup

%build
%make_build

%install
%make_install PREFIX=/usr/

%files
%{_bindir}/%{name}
#%{_datadir}/bash-completion/completions/%{name}
%dir %{_docdir}/%{name}/
%license %{_docdir}/%{name}/copyright
%{_datadir}/help/en_US/%{name}/help.txt
%{_datadir}/help/ru_RU/%{name}/help.txt
%{_datadir}/help/eo/%{name}/help.txt
%attr(0644, root, root) %{_datadir}/man/man1/%{name}.1.gz
%attr(0644, root, root) %{_datadir}/man/ru/man1/%{name}.1.gz
%attr(0644, root, root) %{_datadir}/man/eo/man1/%{name}.1.gz

%changelog
